export const httpCodeToHumanMessage = (codeString: string) => {
  if (codeString === "404") {
    return "No product found";
  }
  if (codeString === "500") {
    return "Server Error";
  }
  return "Error " + codeString;
};
