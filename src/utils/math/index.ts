export const clamp = (
  value: number,
  minVal: number,
  maxVal: number
): number => {
  return Math.min(maxVal, Math.max(minVal, value));
};
