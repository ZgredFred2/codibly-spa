export const removeNonDigits = (s: string) => s.replace(/\D/g, "");
