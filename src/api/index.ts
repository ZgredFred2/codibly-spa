export type ApiParams = {
  id?: number;
  page?: number;
  per_page?: number;
};

export type Product = {
  id: number;
  name: string;
  year: number;
  color: string;
  pantone_value: string;
};

type SupportData = { url: string; text: string };

export type ApiResponseSingleProduct = {
  data: Product;
  support: SupportData;
};

export type ApiResponseArrayOfProducts = {
  page: number;
  per_page: number;
  total: number;
  total_pages: number;
  data: Product[] | Product;
  support: SupportData;
};

export type ApiResponse = ApiResponseSingleProduct | ApiResponseArrayOfProducts;

const paramsToString = (params: ApiParams) => {
  return [
    params.id !== undefined ? "id=" + params.id : undefined,
    params.page !== undefined ? "page=" + params.page : undefined,
    params.per_page !== undefined ? "per_page=" + params.per_page : undefined,
  ]
    .filter(Boolean)
    .join("&");
};

export const getApiUrl = (params: ApiParams = {}) => {
  const paramsString = paramsToString(params);
  const appendix = paramsString.length ? "?" + paramsString : "";
  const res = "https://reqres.in/api/products" + appendix;
  console.log(res);
  return res;
};

type FetchParams = {
  itemsPerPage?: number;
  currentPage?: number;
  idText: string;
};

export const fetchProducts = async ({
  itemsPerPage,
  currentPage,
  idText,
}: FetchParams): Promise<ApiResponse> => {
  const apiParams: ApiParams = {
    per_page: itemsPerPage,
    page: currentPage,
    ...(idText === "" ? {} : { id: parseInt(idText) }),
  };
  const e = await fetch(getApiUrl(apiParams));
  if (!e.ok) {
    throw new Error(e.status.toString());
  }
  const res = await e.json();
  return res;
};

export const toSingleArray = (res: ApiResponse) => {
  if ("length" in res.data) {
    return {
      ...res,
      data: res.data as Product[],
    };
  }
  return {
    ...res,
    data: [res.data],
  };
};

// testing purpose
const res = {
  page: 1,
  per_page: 6,
  total: 12,
  total_pages: 2,
  data: [
    {
      id: 1,
      name: "cerulean",
      year: 2000,
      color: "#98B2D1",
      pantone_value: "15-4020",
    },
    {
      id: 2,
      name: "fuchsia rose",
      year: 2001,
      color: "#C74375",
      pantone_value: "17-2031",
    },
    {
      id: 3,
      name: "true red",
      year: 2002,
      color: "#BF1932",
      pantone_value: "19-1664",
    },
    {
      id: 4,
      name: "aqua sky",
      year: 2003,
      color: "#7BC4C4",
      pantone_value: "14-4811",
    },
    {
      id: 5,
      name: "tigerlily",
      year: 2004,
      color: "#E2583E",
      pantone_value: "17-1456",
    },
    {
      id: 6,
      name: "blue turquoise",
      year: 2005,
      color: "#53B0AE",
      pantone_value: "15-5217",
    },
  ],
  support: {
    url: "https://reqres.in/#support-heading",
    text: "To keep ReqRes free, contributions towards server costs are appreciated!",
  },
} satisfies ApiResponse;
