import React, { Fragment, useEffect, useState } from "react";
import logo from "./logo.svg";
import "./App.css";
import { useQuery } from "@tanstack/react-query";
import { fetchProducts, Product, toSingleArray } from "./api";
import { TableComp } from "components/TableComp";
import { Pagination, Spinner } from "flowbite-react";
import { InputComp } from "components/InputComp";
import { removeNonDigits } from "utils/string";
import { ModalComp } from "components/ModalComp";
import useUrlState from "@ahooksjs/use-url-state";
import { httpCodeToHumanMessage } from "utils/httpCode";

function App() {
  const [{ idText, itemsPerPage, currentPage }, setState] = useUrlState(
    {
      idText: "",
      itemsPerPage: 5,
      currentPage: 1,
    },
    {
      parseOptions: { parseNumbers: true },
      stringifyOptions: { skipEmptyString: true },
    }
  );

  const [productIdModal, setProductIdModal] = useState<number>();

  const products = useQuery({
    queryKey: ["products", idText, currentPage, itemsPerPage],
    queryFn: () =>
      fetchProducts({ idText, currentPage, itemsPerPage }).then(toSingleArray),
    retry: 0,
    refetchOnWindowFocus: false,
    keepPreviousData: true,
  });

  const findProduct = (id: number, products: Product[]) => {
    return products.find((p) => p.id === id);
  };

  useEffect(() => {
    if (productIdModal && products.isSuccess) {
      if (!findProduct(productIdModal, products.data.data)) {
        setProductIdModal(undefined);
      }
    }
  }, [products.data]);

  const handleProductRowClick = (product: Product) => {
    setProductIdModal(product.id);
  };

  return (
    <div className="dark h-full">
      <div className="h-full w-full dark:bg-gray-900 flex flex-col justify-center items-center">
        <div className="p-5 min-h-[600px] dark:text-white justify-start min-w-[400px] items-center flex flex-col">
          <InputComp
            ok={!products.isError}
            inputProps={{
              placeholder: "product id",
              onChange: (e) => {
                setState({ idText: removeNonDigits(e.target.value) });
              },
              value: idText,
            }}
          />

          <Fragment>
            <div className="min-h-[330px]">
              {products.isFetching ? (
                <div className="h-full flex flex-col justify-center items-center">
                  <Spinner color="purple" />
                </div>
              ) : products.isSuccess ? (
                <TableComp
                  products={products.data.data}
                  onClick={handleProductRowClick}
                />
              ) : (
                !!products.error &&
                products.error instanceof Error &&
                httpCodeToHumanMessage(products.error.message)
              )}
            </div>
          </Fragment>
          <Fragment>
            {products.isSuccess && "total_pages" in products.data && (
              <Pagination
                currentPage={currentPage}
                layout="navigation"
                onPageChange={(page) => {
                  setState({ currentPage: page });
                }}
                showIcons={true}
                previousLabel=""
                nextLabel=""
                totalPages={products.data.total_pages}
              />
            )}

            {products.isSuccess && productIdModal && (
              <ModalComp
                show={productIdModal !== undefined}
                product={findProduct(productIdModal, products.data.data)}
                onClose={() => {
                  setProductIdModal(undefined);
                }}
              />
            )}
          </Fragment>
        </div>
      </div>
    </div>
  );
}

export default App;
