import { Product } from "api/index";
import { Button, Modal } from "flowbite-react";
import { FC, Fragment } from "react";

export type ModalCompProps = {
  product?: Product;
  show: boolean;
  onClose?: () => any;
};

export const ModalComp: FC<ModalCompProps> = (props) => {
  const { product } = props;

  if (!props.show || !product) return <Fragment />;

  return (
    <Fragment>
      <Modal
        show={true}
        onClose={() => {
          props.onClose?.();
        }}
        size={"sm"}
        className="dark"
      >
        <Modal.Header className="flex flex-row text-center justify-center items-center w-full">
          Product Info
        </Modal.Header>
        <Modal.Body>
          <div className="space-y-6">
            <div className="text-base leading-relaxed text-gray-500 dark:text-gray-400">
              <div>id: {product.id}</div>
              <div className="capitalize">name: {product.name}</div>
              <div>
                hex color:{" "}
                <span
                  className="text-black"
                  style={{ backgroundColor: product.color }}
                >
                  {product.color}
                </span>
              </div>
              <div>year: {product.year}</div>
              <div>pantone value: {product.pantone_value}</div>
            </div>
          </div>
        </Modal.Body>
        <Modal.Footer>
          <Button
            onClick={() => {
              props.onClose?.();
            }}
          >
            Close
          </Button>
        </Modal.Footer>
      </Modal>
    </Fragment>
  );
};
