import { Product } from "api/index";
import { Table } from "flowbite-react";
import { FC } from "react";

export type TableCompProps = {
  products: Product[];
  onClick?: (product: Product, idx: number) => any;
};

export const TableComp: FC<TableCompProps> = (props) => {
  return (
    <Table hoverable={true}>
      <Table.Head>
        <Table.HeadCell>Product id</Table.HeadCell>
        <Table.HeadCell>Product name</Table.HeadCell>
        <Table.HeadCell>Year</Table.HeadCell>
      </Table.Head>
      <Table.Body className="divide-y">
        {props.products.map((product, idx) => (
          <Table.Row
            key={product.id}
            className="dark:border-gray-700 dark:bg-gray-800 text-black hover:brightness-110 cursor-pointer"
            style={{ backgroundColor: product.color }}
            onClick={() => {
              props.onClick?.(product, idx);
            }}
          >
            <Table.Cell className="whitespace-nowrap font-medium dark:text-white ">
              {product.id}
            </Table.Cell>
            <Table.Cell className="capitalize">{product.name}</Table.Cell>
            <Table.Cell>{product.year}</Table.Cell>
          </Table.Row>
        ))}
      </Table.Body>
    </Table>
  );
};
