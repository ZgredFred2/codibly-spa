<!-- Please update value in the {}  -->

<h1 align="center">Codibly SPA</h1>

[:point_right: Demo](https://zgredfred2.gitlab.io/codibly-spa/)

<!-- TABLE OF CONTENTS -->

## Table of Contents

- [Overview](#overview)
  - [Built With](#built-with)
- [Features](#features)
- [How to use](#how-to-use)

<!-- OVERVIEW -->

## Overview

![screenshot](docs/demo.gif)  
App to search simple product list.

### Initalized With

- [Create React App](https://create-react-app.dev/)

### Built With

<!-- This section should list any major frameworks that you built your project using. Here are a few examples.-->

- [React](https://reactjs.org/)
- [TanStack Query](https://tanstack.com/query/latest)
- [Tailwind](https://tailwindcss.com/)
- [Flowbite React Components](https://flowbite-react.com/)

### Used external API

- [ReqRes](https://reqres.in/)

## Features

<!-- List the features of your application or follow the template. Don't share the figma file here :) -->

This application/site was created based on following statment:

> The goal of the task is to implement SPA application with just one view. You should use the below API endpoint to display the paginated list of products.
> At the top of the view, there should be text input, which allows the user to filter results by id.
> The input should accept only numbers, other signs should not even appear.
>
> Below this input user should see a table displaying the following items’ properties: id, name, and year.
> Additionally, the background colour of each row should be taken from the colour property.
>
> After clicking on a row a modal should be displayed and should present all item data.
>
> The table should display 5 items per page.
> Under the table, there should be a pagination component, which allows switching between pages with “next” and “previous” arrows.
>
> Please remember about handling situations when API endpoint returns a 4XX or 5XX error. In such a case the user should be informed about the error.
>
> Apart from React, the technology stack totally ups to you, the same applies to styling. Your app should start after running npm install & npm start.
>
> Remarks:
>
> - filtering and pagination should be performed within the API, not on the frontend side
> - take into account that API returns 12 items in total
> - after changing the page it should be possible to copy-paste web browser URL to another tab, where this exact page should be displayed on the start

## How To Use

<!-- Example: -->

To clone and run this application, you'll need

- [Git](https://git-scm.com)
- [Node.js](https://nodejs.org/en/download/) (which comes with [npm](http://npmjs.com)) installed on your computer.

From your command line:

```bash
# Clone this repository
git clone https://gitlab.com/ZgredFred2/codibly-spa

# enter weather-app directory
cd codibly-spa

# Install dependencies
npm install

# Run app in development mode
npm start
```
